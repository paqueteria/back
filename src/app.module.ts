import { Module } from '@nestjs/common';
import { PaquetesModule } from './paquetes/paquetes.module';
import { TypegooseModule } from 'nestjs-typegoose';
import { AuthModule } from './auth/auth.module';
import { UsuariosModule } from './usuarios/usuarios.module';
import { SeederModule } from './seeder/seeder.module';

@Module({
  imports: [
    TypegooseModule.forRoot(
      'mongodb://root:paqueteriaPassword@db:27017/admin',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      },
    ),
    PaquetesModule,
    AuthModule,
    UsuariosModule,
    SeederModule,
  ],
})
export class AppModule {}
