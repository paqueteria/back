import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { LoginUsuarioDto } from '../usuarios/dto/login-usuario.dto';
import { UsuarioAccessTokenDto } from './dto/usuario-access-token.dto';
import { UsuariosService } from '../usuarios/usuarios.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly usuariosService: UsuariosService,
    private readonly jwtService: JwtService,
  ) {}

  async validarUsuario(username: string, password: string): Promise<any> {
    return await this.usuariosService.findByNameAndPassword(username, password);
  }

  async login(
    loginUsuarioDto: LoginUsuarioDto,
  ): Promise<UsuarioAccessTokenDto> {
    const usuario = await this.usuariosService.findByName(
      loginUsuarioDto.username,
    );
    if (usuario) {
      const payload = {
        username: usuario.username,
        id: usuario._id,
        rol: usuario.rol,
      };
      return {
        usuarioId: usuario._id,
        access_token: this.jwtService.sign(payload),
        rol: usuario.rol,
      };
    } else {
      throw new NotFoundException(
        `No se encontró el usuario ${loginUsuarioDto.username}`,
      );
    }
  }
}
