import { IsString, IsIn } from 'class-validator';
import { PaqueteTamanio } from '../enum/paquete-tamanio';

export class CreatePaqueteDto {
  @IsString()
  titulo: string;
  @IsString()
  descripcion: string;
  @IsString()
  @IsIn(Object.values(PaqueteTamanio).filter(e => typeof e === 'string'))
  tamanio: string;
}
