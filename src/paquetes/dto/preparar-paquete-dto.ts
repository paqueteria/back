import { IsString, IsIn } from 'class-validator';
import { PaqueteTipoServicio } from '../enum/paquete-tipo-servicio.enum';

export class PrepararPaqueteDto {
  @IsString()
  id: string;
  @IsString()
  domicilio: string;
  @IsString()
  nombreDestinatario: string;
  @IsIn(Object.values(PaqueteTipoServicio).filter(e => typeof e === 'string'))
  tipoServicio: string;
  status: number;
}
