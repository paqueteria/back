import { IsString, IsNumber } from 'class-validator';

export class UpdatePaqueteDto {
  @IsString()
  _id: string;
  @IsNumber()
  status: number;
}
