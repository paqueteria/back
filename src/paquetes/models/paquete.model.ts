import { prop, Typegoose } from 'typegoose';

export class Paquete extends Typegoose {
  _id: string;

  @prop()
  titulo: string;

  @prop()
  descripcion: string;

  @prop({ default: new Date() })
  fechaCreacion: Date;

  @prop({ default: 0 })
  status: number;

  @prop()
  tamanio: string;

  @prop()
  domicilio: string;

  @prop()
  nombreDestinatario: string;

  @prop()
  tipoServicio: string;

  @prop()
  choferAsignadoId: string;
}
