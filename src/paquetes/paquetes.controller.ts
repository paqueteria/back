import {
  Controller,
  Get,
  Body,
  Post,
  Param,
  Query,
  Patch,
} from '@nestjs/common';
import { PaquetesService } from './paquetes.service';
import { CreatePaqueteDto } from './dto/create-paquete.dto';
import { Paquete } from './models/paquete.model';
import { PrepararPaqueteDto } from './dto/preparar-paquete-dto';
import { UpdatePaqueteDto } from './dto/update-paquete.dto';

@Controller('paquetes')
export class PaquetesController {
  constructor(private readonly paquetesService: PaquetesService) {}

  @Get('asignar-paquetes')
  async asignarPaquetes(): Promise<string> {
    return this.paquetesService.asignarPaquetes();
  }

  @Get()
  async findAll(
    @Query('choferAsignado') choferAsignadoId?: string,
    @Query('status') status?: number,
  ): Promise<Paquete[]> {
    return await this.paquetesService.findAll(status, choferAsignadoId);
  }

  @Get(':id')
  async findById(@Param('id') id: string): Promise<Paquete> {
    return await this.paquetesService.findById(id);
  }

  @Post()
  async createPaquete(
    @Body() createPaqueteDto: CreatePaqueteDto,
  ): Promise<Paquete> {
    return this.paquetesService.create(createPaqueteDto);
  }

  @Patch()
  async updatePaquete(
    @Body() updatePaqueteDto: UpdatePaqueteDto,
  ): Promise<Paquete> {
    return await this.paquetesService.update(updatePaqueteDto);
  }

  @Post('preparar')
  async prepararPaquete(
    @Body() prepararPaqueteDto: PrepararPaqueteDto,
  ): Promise<PrepararPaqueteDto> {
    return this.paquetesService.preparar(prepararPaqueteDto);
  }
}
