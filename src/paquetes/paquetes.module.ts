import { Module } from '@nestjs/common';
import { PaquetesController } from './paquetes.controller';
import { PaquetesService } from './paquetes.service';
import { TypegooseModule } from 'nestjs-typegoose';
import { Paquete } from './models/paquete.model';
import { UsuariosModule } from '../usuarios/usuarios.module';

@Module({
  imports: [TypegooseModule.forFeature([Paquete]), UsuariosModule],
  controllers: [PaquetesController],
  providers: [PaquetesService],
  exports: [PaquetesService],
})
export class PaquetesModule {}
