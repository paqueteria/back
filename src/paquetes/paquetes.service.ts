import { Injectable } from '@nestjs/common';
import { CreatePaqueteDto } from './dto/create-paquete.dto';
import { InjectModel } from 'nestjs-typegoose';
import { ModelType } from 'typegoose';
import { Paquete } from './models/paquete.model';
import { PrepararPaqueteDto } from './dto/preparar-paquete-dto';
import { PaqueteStatus } from './enum/paquete-status.enum';
import { UsuariosService } from '../usuarios/usuarios.service';
import { Usuario } from '../usuarios/models/usuario.model';
import { PaqueteTipoServicio } from './enum/paquete-tipo-servicio.enum';
import { UsuarioRol } from '../usuarios/enum/usuario-rol';
import { PaquetesAsignadosChoferDto } from './dto/paquetes-asignados-chofer.dto';
import { PaqueteTamanio } from './enum/paquete-tamanio';
import { UpdatePaqueteDto } from './dto/update-paquete.dto';
import { PAQUETES_SEED } from './seed/paquetes.seed';

@Injectable()
export class PaquetesService {
  constructor(
    @InjectModel(Paquete)
    private readonly paqueteModel: ModelType<Paquete>,
    private readonly usuariosService: UsuariosService,
  ) {}

  async findAll(status: number, choferAsignadoId: string): Promise<any[]> {
    if (choferAsignadoId) {
      return await this.paqueteModel
        .find({
          choferAsignadoId: choferAsignadoId,
        })
        .populate('choferAsignadoId', '', Usuario, { _id: choferAsignadoId });
    }

    if (status) {
      return await this.paqueteModel
        .find({ status: status })
        .populate('choferAsignadoId', '', Usuario, { _id: choferAsignadoId });
    } else {
      return await this.paqueteModel.find();
    }
  }

  async findById(paqueteId: string): Promise<Paquete> {
    return await this.paqueteModel.findById(paqueteId);
  }

  async findByTitulo(titulo: string): Promise<Paquete> {
    return await this.paqueteModel.findOne({
      titulo: titulo,
    });
  }

  async getChoferNumeroPaquetesAsignados(
    choferId: string,
  ): Promise<PaquetesAsignadosChoferDto> {
    const paquetesAsignadosChoferDto = new PaquetesAsignadosChoferDto();
    const paquetesAsignados = await this.paqueteModel
      .find()
      .and([
        { status: PaqueteStatus.PaquetePreparado },
        { choferAsignadoId: choferId },
      ]);

    await Promise.all(
      paquetesAsignados.map(paquete => {
        switch (paquete.tamanio) {
          case PaqueteTamanio.Chico:
            paquetesAsignadosChoferDto.chico += 1;
            break;
          case PaqueteTamanio.Mediano:
            paquetesAsignadosChoferDto.mediano += 1;
            break;
          case PaqueteTamanio.Grande:
            paquetesAsignadosChoferDto.grande += 1;
            break;
        }
      }),
    );
    return paquetesAsignadosChoferDto;
  }

  async create(createPaqueteDto: CreatePaqueteDto): Promise<Paquete> {
    const paqueteCreado = new this.paqueteModel(createPaqueteDto);
    return await paqueteCreado.save();
  }

  async preparar(prepararPaqueteDto: PrepararPaqueteDto): Promise<any> {
    prepararPaqueteDto.status = PaqueteStatus.PaquetePreparado;
    return await this.paqueteModel.findByIdAndUpdate(
      prepararPaqueteDto.id,
      prepararPaqueteDto,
      { new: true },
    );
  }

  async update(updatePaqueteDto: UpdatePaqueteDto): Promise<any> {
    return await this.paqueteModel.findByIdAndUpdate(
      updatePaqueteDto._id,
      updatePaqueteDto,
      { new: true },
    );
  }

  async asignarPaquete(paqueteId: string, choferId: string): Promise<any> {
    return await this.paqueteModel.findByIdAndUpdate(
      paqueteId,
      { choferAsignadoId: choferId, status: PaqueteStatus.PaqueteAsignado },
      { new: true },
    );
  }

  async asignarPaquetes(): Promise<any> {
    const choferesDisponibles: Usuario[] = await this.usuariosService.findChoferesDisponibles();
    const paquetesPreparados: Paquete[] = await this.paqueteModel.find({
      status: PaqueteStatus.PaquetePreparado,
    });

    paquetesPreparados.map(async (paquete: Paquete) => {
      const tamanioPaquete = paquete.tamanio;
      if (paquete.tipoServicio === PaqueteTipoServicio.Normal) {
        await Promise.all(
          choferesDisponibles
            .filter(usuario => usuario.rol === UsuarioRol.Chofer)
            .map(async (chofer: Usuario) => {
              const paquetesAsignadosChoferDto = await this.getChoferNumeroPaquetesAsignados(
                chofer._id,
              );
              let puedeLlevarPaquete = false;
              switch (tamanioPaquete) {
                case PaqueteTamanio.Chico:
                  if (paquetesAsignadosChoferDto.chico < 7) {
                    puedeLlevarPaquete = true;
                  }
                  break;
                case PaqueteTamanio.Mediano:
                  if (paquetesAsignadosChoferDto.mediano < 4) {
                    puedeLlevarPaquete = true;
                  }
                  break;
                case PaqueteTamanio.Grande:
                  if (paquetesAsignadosChoferDto.grande < 2) {
                    puedeLlevarPaquete = true;
                  }
                  break;
              }
              if (puedeLlevarPaquete) {
                await this.asignarPaquete(paquete._id, chofer._id);
              }
            }),
        );
      } else {
        await Promise.all(
          choferesDisponibles
            .filter(usuario => usuario.rol === UsuarioRol.ChoferElite)
            .map(async (chofer: Usuario) => {
              const paquetesAsignadosChoferDto = await this.getChoferNumeroPaquetesAsignados(
                chofer._id,
              );
              let puedeLlevarPaquete = false;
              switch (tamanioPaquete) {
                case PaqueteTamanio.Chico:
                  if (paquetesAsignadosChoferDto.chico < 10) {
                    puedeLlevarPaquete = true;
                  }
                  break;
                case PaqueteTamanio.Mediano:
                  if (paquetesAsignadosChoferDto.mediano < 3) {
                    puedeLlevarPaquete = true;
                  }
                  break;
                case PaqueteTamanio.Grande:
                  if (paquetesAsignadosChoferDto.grande < 1) {
                    puedeLlevarPaquete = true;
                  }
                  break;
              }
              if (puedeLlevarPaquete) {
                await this.asignarPaquete(paquete._id, chofer._id);
              }
            }),
        );
      }
    });
    return '';
  }

  async seedPaquetes(): Promise<any> {
    return PAQUETES_SEED.map(async createPaquete => {
      const paqueteEncontrado = await this.findByTitulo(createPaquete.titulo);
      if (!paqueteEncontrado) {
        const paqueteCreado = new this.paqueteModel(createPaquete);
        return await paqueteCreado.save();
      }
    });
  }
}
