import { PaqueteTamanio } from '../enum/paquete-tamanio';

export const PAQUETES_SEED = [
  {
    titulo: 'Caja Pañal Huggies',
    descripcion: 'Caja Pañal Huggies Ultraconfort E2 Con 6 Paquetes (240 Pzas)',
    tamanio: PaqueteTamanio.Chico,
  },
  {
    titulo: 'Original Xiaomi Mi Band ',
    descripcion: 'Original Xiaomi Mi Band 4 Pulsera Inteligente Internacional',
    tamanio: PaqueteTamanio.Chico,
  },
  {
    titulo: 'Carpa Toldo 3x3',
    descripcion: 'Carpa Toldo 3x3 Reforzado Plegable',
    tamanio: PaqueteTamanio.Grande,
  },
  {
    titulo: 'Microondas Whirlpool',
    descripcion: 'Microondas Whirlpool WM1211D plata 110V',
    tamanio: PaqueteTamanio.Mediano,
  },
];
