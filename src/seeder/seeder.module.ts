import { Module } from '@nestjs/common';
import { SeederService } from './seeder.service';
import { UsuariosModule } from '../usuarios/usuarios.module';
import { PaquetesModule } from '../paquetes/paquetes.module';

@Module({
  imports: [UsuariosModule, PaquetesModule],
  providers: [SeederService],
  exports: [SeederService],
})
export class SeederModule {}
