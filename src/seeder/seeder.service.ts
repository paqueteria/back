import { Injectable } from '@nestjs/common';
import { UsuariosService } from '../usuarios/usuarios.service';
import { PaquetesService } from '../paquetes/paquetes.service';

@Injectable()
export class SeederService {
  constructor(
    private readonly usuariosService: UsuariosService,
    private readonly paquetesService: PaquetesService,
  ) {}

  async seed() {
    await this.usuariosService.seedUsuarios();
    await this.paquetesService.seedPaquetes();
  }
}
