import { IsString, IsIn } from 'class-validator';
import { UsuarioRol } from '../enum/usuario-rol';

export class CreateUsuarioDto {
  @IsString()
  username: string;
  @IsString()
  password: string;
  @IsIn(Object.values(UsuarioRol).filter(e => typeof e === 'string'))
  rol: string;
}
