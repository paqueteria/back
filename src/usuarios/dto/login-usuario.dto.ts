import { IsString, IsIn } from 'class-validator';
import { UsuarioRol } from '../enum/usuario-rol';

export class LoginUsuarioDto {
  @IsString()
  username: string;
  @IsString()
  password: string;
}
