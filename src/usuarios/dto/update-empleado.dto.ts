import { IsString } from 'class-validator';

export class UpdateUsuarioDto {
  @IsString()
  _id: string;
  @IsString()
  username: string;
  @IsString()
  password: string;
  @IsString()
  rol: string;
}
