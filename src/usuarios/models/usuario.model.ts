import { prop, Typegoose } from 'typegoose';

export class Usuario extends Typegoose {
  _id: string;
  @prop()
  username: string;
  @prop()
  password: string;
  @prop()
  rol: string;
}
