import { UsuarioRol } from '../enum/usuario-rol';

export const USUARIOS_SEED = [
  { username: 'admin', password: '123', rol: UsuarioRol.Admin },
  {
    username: 'bodeguero1',
    password: '123',
    rol: UsuarioRol.Bodeguero,
  },
  {
    username: 'bodeguero2',
    password: '123',
    rol: UsuarioRol.Bodeguero,
  },
  {
    username: 'chofer1',
    password: '123',
    rol: UsuarioRol.Chofer,
  },
  {
    username: 'chofer2',
    password: '123',
    rol: UsuarioRol.Chofer,
  },
  {
    username: 'choferElite1',
    password: '123',
    rol: UsuarioRol.ChoferElite,
  },
  {
    username: 'choferElite2',
    password: '123',
    rol: UsuarioRol.ChoferElite,
  },
];
