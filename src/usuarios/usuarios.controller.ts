import {
  Controller,
  Get,
  Body,
  Post,
  Param,
  UseGuards,
  Patch,
  Delete,
} from '@nestjs/common';
import { Usuario } from './models/usuario.model';
import { UsuariosService } from './usuarios.service';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../auth/auth.service';
import { LoginUsuarioDto } from './dto/login-usuario.dto';
import { User } from './decorator/usuario.decorator';
import { UpdateUsuarioDto } from './dto/update-empleado.dto';

@Controller('usuarios')
export class UsuariosController {
  constructor(
    private readonly usuariosService: UsuariosService,
    private readonly authService: AuthService,
  ) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Body() loginUsuarioDto: LoginUsuarioDto) {
    return this.authService.login(loginUsuarioDto);
  }

  @Get()
  async findAll(@User() usuario): Promise<Usuario[]> {
    return await this.usuariosService.findAll();
  }

  @Get(':id')
  async findById(@Param('id') id: string): Promise<any> {
    return await this.usuariosService.findById(id);
  }

  @Post()
  async createUsuario(
    @Body() createUsuarioDto: CreateUsuarioDto,
  ): Promise<Usuario> {
    return await this.usuariosService.create(createUsuarioDto);
  }

  @Patch()
  async updateUsuario(
    @Body() updateUsuarioDto: UpdateUsuarioDto,
  ): Promise<Usuario> {
    return await this.usuariosService.update(updateUsuarioDto);
  }

  @Delete(':id')
  async deleteUsuario(@Param('id') id: string): Promise<Usuario> {
    return await this.usuariosService.deleteById(id);
  }
}
