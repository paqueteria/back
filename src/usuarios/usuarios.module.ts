import { Module, forwardRef } from '@nestjs/common';
import { UsuariosController } from './usuarios.controller';
import { TypegooseModule } from 'nestjs-typegoose';
import { Usuario } from './models/usuario.model';
import { UsuariosService } from './usuarios.service';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    TypegooseModule.forFeature([Usuario]),
    forwardRef(() => AuthModule),
  ],
  providers: [UsuariosService],
  controllers: [UsuariosController],
  exports: [UsuariosService],
})
export class UsuariosModule {}
