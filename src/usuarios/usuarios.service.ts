import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { ModelType } from 'typegoose';
import { Usuario } from './models/usuario.model';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { USUARIOS_SEED } from './seed/usuarios.seed';
import { UsuarioRol } from './enum/usuario-rol';
import { UpdateUsuarioDto } from './dto/update-empleado.dto';
@Injectable()
export class UsuariosService {
  constructor(
    @InjectModel(Usuario)
    private readonly usuarioModel: ModelType<Usuario>,
  ) {}

  async findAll(): Promise<Usuario[]> {
    return await this.usuarioModel.find();
  }

  async findById(usuarioId: string): Promise<any> {
    return await this.usuarioModel.findById(usuarioId);
  }

  async deleteById(usuarioId: string): Promise<any> {
    return await this.usuarioModel.findByIdAndDelete(usuarioId);
  }

  async findByName(username: string): Promise<Usuario> {
    return await this.usuarioModel.findOne({
      username: username,
    });
  }

  async findChoferesDisponibles(): Promise<any> {
    return this.usuarioModel
      .find()
      .or([{ rol: UsuarioRol.ChoferElite }, { rol: UsuarioRol.Chofer }]);
  }

  async findByNameAndPassword(
    username: string,
    password: string,
  ): Promise<Usuario> {
    return await this.usuarioModel.findOne({
      username: username,
      password: password,
    });
  }

  async create(createUsuarioDto: CreateUsuarioDto): Promise<Usuario> {
    const usuarioEncontrado = await this.usuarioModel.findOne({
      username: createUsuarioDto.username,
    });
    if (usuarioEncontrado) {
      throw new BadRequestException(
        `El usuario: ${usuarioEncontrado.username} ya esta registrado`,
      );
    } else {
      const usuarioCreado = new this.usuarioModel(createUsuarioDto);
      return await usuarioCreado.save();
    }
  }

  async update(updateUsuarioDto: UpdateUsuarioDto): Promise<Usuario> {
    return await this.usuarioModel.findByIdAndUpdate(
      updateUsuarioDto._id,
      updateUsuarioDto,
      { new: true },
    );
  }

  async seedUsuarios(): Promise<any> {
    return USUARIOS_SEED.map(async (createUsuarioDto: CreateUsuarioDto) => {
      const usuarioEncontrado = await this.findByName(
        createUsuarioDto.username,
      );
      if (!usuarioEncontrado) {
        const usuarioCreado = new this.usuarioModel(createUsuarioDto);
        return await usuarioCreado.save();
      }
    });
  }
}
